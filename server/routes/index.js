const express = require("express");
const router = express.Router();
const mongo = require("./../connection");
var formidable = require('formidable');
var mv = require('mv');

router.post("/insert", async function (req, res) {
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    var doc = JSON.parse(fields.document);
    var oldpath = files.file.path;
    var newpath = "./files/" + files.file.name;
    mv(oldpath, newpath, { clobber: false }, function (err) {
      if (err) {
        console.log(err);
        res.send("file already exist");
      }
      else {
        var data = {
          KEY: [doc.key1, doc.key2, doc.key3, doc.key4],
          TITLE: doc.title,
          file: [newpath]
        }
        console.log(data);
        const db1 = mongo.get().collection("files");
        db1.insert(data, (err, result) => {
          if (err) {
            console.log(error);
          }
          else {
            res.send("updated Sucess")
          }
        });        
      }
    });
    
  });
});

router.post("/find", async function (req, res) {
  const db = mongo.get().collection("files");
  db.find({
    "KEY":
      {
        $all: [req.body.KEY]
      }
  }).toArray((err, result) => {
    if (err) console.log(err);
    else {
      console.log(result);
      res.send(result);
    }
  })
})

router.get("/display",function(req,res){
  res.writeHead(200, {'Content-Type': 'text/plain'});
  // res.send(req.params.src);
  // res.end();
})

router.post("/loginCheck", async function (req, res) {
  const db = mongo.get().collection("admin");
  db.find({ _id: req.body._id }).toArray((err, result) => {
    console.log(result[0])
    if (result[0].pswd == req.body.pswd) {
      res.send("correct");
    }
    else {
      res.send("wrong");
    }
  })

})

module.exports = router;
