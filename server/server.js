const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const mongo = require("./connection");
const morgan = require("morgan");
const routes = require("./routes/index");
app.use(express.static('files'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(morgan("combined"));

app.listen(5000, () => {
  console.log("server is running on port 5000!");
});

mongo.connect(function(err, client) {
  if (err) console.log(err);
  else {
    console.log("conneted successful");
  }
});

app.use("/files", routes);
