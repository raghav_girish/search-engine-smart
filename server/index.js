const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
// const mongo = require("./connection");
const morgan = require("morgan");
var formidable = require('formidable');
var mv = require('mv');
// const routes = require("./routes/index");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(morgan("combined"));

app.listen(5000, () => {
  console.log("server is running on port 5000!");
});

// mongo.connect(function(err, client) {
//   if (err) console.log(err);
//   else {
//     console.log("conneted successful");
//   }
// });

app.post('/insert', function (req, res) {
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    //console.log(JSON.parse(fields.document))
    var doc=JSON.parse(fields.document);
    var oldpath = files.file.path;
    var newpath = "./files/" + files.file.name;
    mv(oldpath, newpath, { clobber: false }, function (err) {
      if (err) {
        console.log(err);
        res.send("file already exist");
      }
      else res.send("updated Sucess")
    });
    var data={
      KEY:[doc.key1,doc.key2,doc.key3,doc.key4],
      TITLE:doc.title,
      doamin:doc.domain,
      file:[newpath]
    }
    console.log(data);
  });
})
// app.use("/files", routes);
