import React from 'react';
import Paper from '@material-ui/core/Paper';
import { Box, TextField } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import axios from 'axios';

class Upload extends React.Component {
    state = {
        file: null,
        key1: "",
        key2: "",
        key3: "",
        key4: "",
        title: ""
    }
    handleChange = e => {
        if (e.currentTarget.name === 'file')
            this.setState({
                [e.currentTarget.name]: e.currentTarget.files[0]
            })
        else {
            console.log(e.currentTarget.value)
            this.setState({
                [e.currentTarget.name]: e.currentTarget.value
            })
        }
    }
    onClickHandler = () => {
        var documentJson = {
            key1: this.state.key1,
            key2: this.state.key2,
            key3: this.state.key3,
            key4: this.state.key4,
            title: this.state.title
        }
        const data = new FormData();
        data.append('file', this.state.file);
        data.append("document", JSON.stringify(documentJson));
        console.log(data);
        axios.post("http://localhost:5000/files/insert", data)
            .then(res => {
                console.log(res.data)
                if(res.data==='updated Sucess')
                {
                    alert("Updated Sucess");
                }
                else 
                {
                    alert("Already Exist");
                }
            })

    }
    render() {
        console.log(this.state);
        return (
            <div>
                <Box p={5}>
                    <Paper elevation={3} align="center">
                        <Box p={5}>
                            <TextField label="Title" name="title" style={{ marginRight: "1%", width: "45%" }} onChange={this.handleChange} />
                            <br /><br />
                            <TextField label="Key-1" name="key1" style={{ marginRight: "1%" }} onChange={this.handleChange} />
                            <TextField label="Key-2" name="key2" style={{ marginRight: "1%" }} onChange={this.handleChange} />
                            <TextField label="Key-3" name="key3" style={{ marginRight: "1%" }} onChange={this.handleChange} />
                            <TextField label="Key-4" name="key4" style={{ marginRight: "1%" }} onChange={this.handleChange} />
                            <br /><br />
                            <TextField lable="file" name="file" type="file" variant="outlined" onChange={this.handleChange} />
                            <br /><br />
                            <Button variant="contained" color="primary" onClick={this.onClickHandler}>
                                Upload
                            </Button>
                        </Box>
                    </Paper>
                </Box>

            </div>
        )
    }
}

export default Upload;

