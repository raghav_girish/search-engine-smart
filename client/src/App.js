import React from 'react';
import Upload from './component/Upload';
import Search from './component/Search';
import { AppBar } from '@material-ui/core';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

class App extends React.Component {

  render() {
    console.log(this.state);
    return (
      <div>
<AppBar position="static">
  <Toolbar>
    <IconButton edge="start" color="inherit" aria-label="menu">
      <MenuIcon />
    </IconButton>
    <Typography variant="h6" >
      KRCE Search Engine
      </Typography>
  </Toolbar>
</AppBar>
<br />
<Upload/>
<Search/>
      </div>
    )
  }
}

export default App;


// import React, { Component } from 'react';
// import SpeechRecognition from 'react-speech-recognition';
// import Upload from './component/Upload';
// import Search from './component/Search';
// import { AppBar } from '@material-ui/core';
// import Toolbar from '@material-ui/core/Toolbar';
// import Typography from '@material-ui/core/Typography';
// import IconButton from '@material-ui/core/IconButton';
// import MenuIcon from '@material-ui/icons/Menu';

// const options = {
//   autoStart: false
// }

// class App extends Component {
//   render() {
    // const { transcript, resetTranscript, browserSupportsSpeechRecognition,startListening,stopListening} = this.props

//     if (!browserSupportsSpeechRecognition) {
//       return null
//     }
    
//     console.log(this.state);
//     console.log(transcript);
//     return (
//       <div>
//         <AppBar position="static">
//           <Toolbar>
//             <IconButton edge="start" color="inherit" aria-label="menu">
//               <MenuIcon />
//             </IconButton>
//             <Typography variant="h6" >
//               KRCE Search Engine
//               </Typography>
//           </Toolbar>
//         </AppBar>
//         <br />
//         <Upload />
        // <button onClick={resetTranscript}>Reset</button>
        // <button onClick={startListening}>Start</button>
        // <button onClick={stopListening}>Stop</button>
//         <Search />
//         <span>{transcript}</span>
//       </div>
//     )
//   }
// }

// export default SpeechRecognition(options)(App);
